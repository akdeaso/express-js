const express = require("express");
const app = express();
const log = require("./Middleware/logger");

//Middlewares
app.use(log);
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//Routes
const routes = require("./routes");
app.use("/", routes);

//Server listening
const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
