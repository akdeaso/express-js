const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.send({
    page: "Home",
    message: "Welcome to Home page, use /about, /product, to see other page",
  });
});

router.get("/about", (req, res) => {
  res.send("<h1>Hello this is about page</h1>");
});

const productData = [
  { id: 1, name: "Xiaomi 12T", price: 6000000 },
  { id: 2, name: "Asus ROG 6", price: 25000000 },
  { id: 3, name: "ROG Ally", price: 15000000 },
];

router.get("/product", (req, res) => {
  res.json(productData);
});

router.get("/product/:id", (req, res) => {
  const { id } = req.params;
  const product = productData.find((item) => item.id === parseInt(id));
  if (product) {
    const { name, price } = product;
    res.json({ id, name, price });
  } else {
    res.status(404).json({ message: "Product not found" });
  }
});

module.exports = router;
